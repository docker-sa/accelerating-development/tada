package main

import (
	"context"
	"testing"

	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/require"
	"github.com/testcontainers/testcontainers-go"
	redisModule "github.com/testcontainers/testcontainers-go/modules/redis"
)

func TestGreeting(t *testing.T) {
	ctx := context.Background()

	redisContainer, err := redisModule.RunContainer(ctx, testcontainers.WithImage("redis:7.2.4"))
	require.NoError(t, err)

	endpoint, err := redisContainer.Endpoint(ctx, "")
	require.NoError(t, err)

	rdb := redis.NewClient(&redis.Options{
		Addr: endpoint,
	})

	message := "Hey Bob, what's up?"

	// save a greeting message
	_, err = rdb.Set(ctx, "greetings", message, 0).Result()
	require.NoError(t, err)

	// check the return value of the greetings method
	v := greetings(ctx, rdb)
	require.Equal(t, message, v)
}
