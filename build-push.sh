#!/bin/bash
TAG=0.1.0
docker buildx build \
--platform=linux/arm64 \
--push -t philippecharriere494/accelerate:${TAG} .

docker pull philippecharriere494/accelerate:${TAG}
docker images | grep accelerate

# --platform=linux/amd64,linux/arm64 \
