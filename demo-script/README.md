# Demo

## Prerequisites

- Run `./raz.sh` to reset the environment
- Clear the last hour browsing history
- `helm uninstall traefik`

## What is this application
First, Explain the application
- what is the application
- what is the purpose of the application
- what is the architecture of the application
- what is the technology stack of the application
- what is the deployment strategy of the application
== Show the code of the application

**I want to Dockerize my application**

## Docker Init 

Then, run `docker init`

### Open the generated Dockerfile

- Explain the Dockerfile

- Change `FROM alpine:latest AS final` to `FROM alpine:3.17.2 AS final
> because it's a best practice

- After the `USER appuser` instruction, replace the `COPY` instruction by these lines:
```Dockerfile
WORKDIR /app
# copy and give the rights to the user
COPY --chown=appuser public ./public 
COPY --from=build /bin/server .
```
> because your application is not a simple binary, but a binary and a folder (`public`) that needs to be copied to serve a web application.

> The `--chown=appuser` option is used to give the rights to the user `appuser` to the copied files.

And at the bottom of the Dockerfile:
```Dockerfile
#ENTRYPOINT [ "/bin/server" ]
ENTRYPOINT [ "./server" ]
```

### Open the generated compose.yaml file

- Explain the compose file
- Add the following lines to after the`build` section:
```yaml
    environment:
      - MESSAGE=🎉 Hello from 🐳 Compose 👋
      - REDIS_URL=redis-server:6379
```

### Redis part

Add this at the bottom of the compose file:
```yaml
  bulk-loading:
    image: redis:7.2.4
    entrypoint: ["/load-data/bulk_loading.sh"]
    volumes:
      - ./load-data:/load-data
    depends_on:
      redis-server:
        condition: service_started

  redis-server:
    image: redis:7.2.4
    environment: 
      - REDIS_ARGS=--save 30 1
    volumes:
      - redis-data:/data
    ports:
      - 6379:6379

volumes:
  redis-data:
```

### Start the application

Run `docker compose up` 
> or `docker compose up --build` to rebuild the image

Switch to **Docker Desktop** to show the running containers

Open a browser and go to `http://localhost:8080` to show the web application

## Docker compose Watch

Stop the application by pressing `Ctrl+C`

### Update the compose file

Add this section
```yaml
    develop:
      watch:
        - action: sync
          path: ./public
          target: /app/public
        - action: rebuild
          path: ./Dockerfile
        - action: rebuild
          path: ./main.go
```
👋 **Explain**

### Open the generated .dockerignore file

Comment this line: `**/Dockerfile*`
> if we are making change in watch mode (for demoing scout, for example)

### Restart the application in "watch" mode

```bash
docker compose watch
```
Switch to **Docker Desktop** to show the running containers

#### ✋ Do some changes in the `public` folder

To trigger the sync action

> `public/components/App.js`

#### ✋ Do some changes in the `main.go` file

To trigger the rebuild action

> `./main.go`


Make a change with `log.Println("📝🙂 message: " + message)`
**Go to the log panel of the running container to show the logs (in Docker Desktop) to show the changes**

## Docker Scout

- Dont' stop the application
- Go back to the Docker Desktop GUI
- Show the analysis view of image

### Vulnerability with the stdlib package
We have a critical vulnerability with the stdlib package with Golang `1.22.1``

Change this line in the Dockerfile (at the begining of the file):

```Dockerfile
ARG GO_VERSION=1.22.1
```

by this one:

```Dockerfile
#ARG GO_VERSION=1.22.4
ARG GO_VERSION=1.22.5
```

📦 It will tigger a rebuild of the image

👋🐳 Go back to the Docker Desktop GUI to show the analysis view of the new image

**we fixed a vulnerability 🎉**

### Vulnerability with the base image

no try the recommended fixes for the based image (for example: `3.20.1`)

👋🐳 Go back to the Docker Desktop GUI to show the analysis view of the new image

😢 **Better but we have still a critical vulnerability**

### Fix the vulnerability by using the `scratch` image
> Explain what is the `scratch` image
💡 -> and what if I can ask what is it to my application? (RAG+Ollama)

Change:
```Dockerfile
FROM alpine:3.20.1 AS final
```

by:
```Dockerfile
FROM alpine:3.20.1 AS certificates
```

Remove the creation of the user (because we are using the `scratch` image)

Add the new stage:
```Dockerfile
FROM scratch AS final
COPY --from=certificates /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
```

Change:
```Dockerfile
COPY --chown=appuser public ./public
```

by:
```Dockerfile
COPY public ./public 
```

👋🐳 Go back to the Docker Desktop GUI to show the analysis view of the new image

🎉🎉🎉 No more vulnerability!!!🙂

Stop the **watch** mode

Restart the application with `docker compose up --build`


But my application is in production and I need to fix the "**silly message**"
`🚧 this is a work in progress 👷‍♂️`

## Docker Exec ---> Docker Debug

👋🐳 Go back to the Docker Desktop GUI

Go to the exec panel 🤯 => you can do nothing! It's a scratch image

Activate the debug mode:
```bash
ls
cd app/public
ls
install micro
micro info.txt
```

🎉🌍 Refresh the page in the browser

🎉 It works!

## Test the Redis part with Testcontainers
>✋ It's a work in progress 🚧

- Look at the `main_test.go` file
- Look at the `greeting` function

```bash
go test
```


## Docker build on kubernetes

> ✋ It's a work in progress 🚧

## Deploy on Kubernetes

✋✋✋ **Don't forget to build and publish on the Hub**

### Checks

```bash
kubectl config get-contexts
# To switch to the docker-desktop context, use:
kubectl config use-context docker-desktop
# Verify
kubectl cluster-info

```

```bash
k9s --all-namespaces
```
### Install Traefik

```bash
helm repo add traefik https://traefik.github.io/charts
helm repo update
helm install traefik traefik/traefik

kubectl get services --all-namespaces
# Run K9S in a terminal
k9s --all-namespaces

```

### Create namespace

```bash
# Type the following commands in another terminal
# Create a namespace
kubectl create namespace demo --dry-run=client -o yaml | kubectl apply -f -
```

### Deploy Redis

#### Create a volume
```bash
# TODO
```

#### Deploy Redis
```bash
kubectl apply -f ./deploy.redis.to.kube.yaml -n demo
#kubectl delete -f ./deploy.redis.to.kube.yaml -n demo

# Only for testing
kubectl apply -f ./deploy.pod.redis.cli.yaml -n demo
#kubectl delete -f ./deploy.pod.redis.cli.yaml -n demo
# Enter, then: redis-cli -h redis-server

```
> Enter into the pod to add some data: `set greetings "👋 Hello from Redis on K8S"`


### Deploy the application

```bash

# Deploy the service
# Open the manifest and explain the content
kubectl apply -f ./deploy.app.to.kube.yaml -n demo

kubectl describe ingress demo-accelerate -n demo

# open the webapp in the browser: http://accelerate.0.0.0.0.nip.io


# Change the number of replicas for demo-accelerate and apply again
# Then refresh the page (several times)

# Change the environment variable (MESSAGE) for demo-tiny-two and apply again
# Then refresh the page

kubectl delete -f ./deploy.app.to.kube.yaml -n demo
# Or:
kubectl delete namespace demo

```


## Deploy on Kubernetes with Compose Bridge
> - download the binary: https://github.com/docker/compose-bridge-binaries/releases
> - add the binary to the PATH (rename it to `compose-bridge`)


### Install Compose Bridge
```bash
export PATH="$HOME/compose:$PATH"
```

Update the compose file

```yaml
  server:
    #build:
    #  context: .
    #  target: final
    image: philippecharriere494/accelerate:0.0.1
```

### Convert the compose file
```bash
compose-bridge convert
```

### Add Ingress

In `out/base` create a file `server-ingress.yaml` with the following content:

```yaml
# Ingress
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
    name: server
    namespace: tada # Change the namespace 👋
    labels:
        com.docker.compose.project: tada # Change the name 👋
        com.docker.compose.service: server
spec:
  rules:
    - host: accelerate.0.0.0.0.nip.io
      http:
        paths:
        - path: /
          pathType: Prefix
          backend:
            service: 
              name: server-published              
              port: 
                name: server-8080


```

### Update `kustomization.yaml`

Add `server-ingress.yaml` to the resources list

```yaml
#! kustomization.yaml
# Generated code, do not edit
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
    - 0-init-hello-go-3-namespace.yaml
    - default-network-policy.yaml
    - server-deployment.yaml
    - server-expose.yaml
    - server-service.yaml
    - server-ingress.yaml # 👋
```

### Then deploy the application

```bash
kubectl apply -k ./out/overlays/desktop
kubectl describe ingress server -n tada
#kubectl delete -k ./out/overlays/desktop

# http://accelerate.0.0.0.0.nip.io

```


## Test containers

